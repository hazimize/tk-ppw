from django.shortcuts import render, redirect
from .models import *
from .forms import formulir_testimoni
from django.shortcuts import render, redirect
from . import forms, models

def formulir_testimoni(request):
    if(request.method == "POST"):
        context = forms.formulir_testimoni(request.POST)
        if (context.is_valid()):
            context1 = models.Testimoni()
            context1.nama = context.cleaned_data["nama"]
            context1.pekerjaan = context.cleaned_data["pekerjaan"]
            context1.ulasan = context.cleaned_data["ulasan"]
            context1.save()
        return redirect("/testimoni")
    else:
        context = forms.formulir_testimoni()
        context1 = models.Testimoni.objects.all()
        context_dictio = {
            'formulir' : context,
            'testimoni' : context1
        }
    return render(request, 'testimoni.html', context_dictio)


def display_testimoni(request):
    form= Testimoni.objects.all()
    response={'form':form}
    return render(request, 'testimoni1.html', response)
