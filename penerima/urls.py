from django.urls import path

from . import views


app_name = 'penerima'

urlpatterns = [
    path('', views.pendaftar_form, name='pendaftar_form'),
    path('donatur/', views.display_form, name='display_form'),
    path('terimakasih/', views.thanks, name='thanks')
]
